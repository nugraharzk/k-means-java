package id.skripsi;

import java.util.ArrayList;
import java.util.List;

public class Cluster {

    public List<Document> documents;
    public Document centroid;
    public int id;

    //Creates a new Cluster
    public Cluster(int id) {
        this.id = id;
        this.documents = new ArrayList();
        this.centroid = null;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void addDocument(Document document) {
        documents.add(document);
    }

    public void setDocuments(List documents) {
        this.documents = documents;
    }

    public Document getCentroid() {
        return centroid;
    }

    public void setCentroid(Document centroid) {
        this.centroid = centroid;
    }

    public int getId() {
        return id;
    }

    public void clear() {
        documents.clear();
    }

    public void plotCluster() {
        System.out.println("[Cluster: " + id+"]");
        System.out.println("[Centroid: " + centroid + "]");
        System.out.println("[Documents: ");
        for(Document p : documents) {
            System.out.println(p);
        }
        System.out.println("]");
    }

    @Override
    public String toString() {
        return "Cluster "+id;
    }
}

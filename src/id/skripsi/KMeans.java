package id.skripsi;

import java.io.*;
import java.util.*;

public class KMeans {

    // Jumlah cluster yang akan dibentuk
    private int NUM_CLUSTERS = 2;
    private int MAX_ITERATION = 50;
    private int TOLERANCE = 3;

    private List<Document> documents = new ArrayList<>();
    private List<Cluster> clusters = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        KMeans kmeans = new KMeans();
        kmeans.init();
        kmeans.calculate();
    }

    private void setInput() {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("src/data kategorikal.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int i = 0;
        for (List<String> strings : records) {
            int j = 0;
            if (i > 0) {
                Document document = new Document();
                for (String a : strings) {
                    if (j == 0) {
                        document.setData(a);
                    }
                    else if (j == 1) {
                        document.setInstansi(a);
                    }
                    else if (j == 2) {
                        document.setStatus(a);
                    }
                    else if (j == 3) {
                        document.setAlumni(a);
                    }
                    else if (j == 4) {
                        document.setGaji(a);
                    }
                    else if (j == 5) {
                        document.setLokasi(a);
                    }
                    else if (j == 6) {
                        document.setBidang(a);
                    }
                    j++;
                }
                documents.add(document);
            }
            i++;
        }
    }

    private void init() {
        //Create Points
        setInput();

        //Create Clusters
        //Set Random Centroids
//        System.out.println(documents);
        setRandomCentroids();
//        System.out.println(documents);

        //Print Initial state
        System.out.println("\nInitial state");
        plotClusters();
        System.out.println("\n");
    }

    private void setRandomCentroids() {
        Random rand = new Random();
        List<Document> temp = new ArrayList<>();
        temp = documents;

        for (int i = 0; i < NUM_CLUSTERS; i++) {
            int randomIndex = rand.nextInt(temp.size());
            Cluster cluster = new Cluster(i);
            Document randomElement = temp.get(randomIndex);
            temp.remove(randomIndex);
            cluster.setCentroid(randomElement);
            clusters.add(cluster);
        }

    }

    private void calculate() {
        boolean finish = false;
        int iteration = 0;
        int localTolerance = 0;

        // Add in new data, one at a time, recalculating centroids with each new one.
        while(!finish && iteration < MAX_ITERATION) {
            //Clear cluster state
            clearClusters();

            List<Document> lastCentroids = getCentroids();

            //Assign points to the closer cluster
            assignCluster();

            iteration++;
            System.out.println("#################");
            System.out.println("Iteration: " + iteration);
            System.out.println("#################");
            plotClusters();

            //Calculate new centroids.
            calculateCentroids();

            List<Document> currentCentroids = getCentroids();

            //Calculates total distance between new and old Centroids
            double distance = 0;
            for(int i = 0; i < lastCentroids.size(); i++) {
                distance += Document.distance(lastCentroids.get(i),currentCentroids.get(i));
            }

            if(distance == 0 && localTolerance < TOLERANCE) {
                localTolerance++;
            } else if (distance == 0 && localTolerance == TOLERANCE) {
                System.out.println("\n########################");
                System.out.println("K-Means konvergen pada iterasi ke-"+iteration);
                System.out.println("########################");
                finish = true;
            }
        }
        if (iteration == MAX_ITERATION) {
            System.out.println("\n########################");
            System.out.println("K-Means belum konvergen tetapi telah mencapai maksimum iterasi");
            System.out.println("########################");
        }
    }

    private void plotClusters() {
        for (int i = 0; i < NUM_CLUSTERS; i++) {
            Cluster c = clusters.get(i);
            c.plotCluster();
        }
    }

    private void clearClusters() {
        for(Cluster cluster : clusters) {
            cluster.clear();
        }
    }

    private List<Document> getCentroids() {
        List<Document> centroids = new ArrayList<>();
        for(Cluster cluster : clusters) {
            Document aux = cluster.getCentroid();
            centroids.add(aux);
        }
        return centroids;
    }

    private void assignCluster() {
        double max = Double.MAX_VALUE;
        double min = max;
        int cluster = 0;
        double distance = 0.0;

        System.out.println(
                "##############################\n"
                +"Assign Cluster Disini\n"
                +"##############################\n");

        for(Document document : documents) {
            min = max;
            for(int i = 0; i < NUM_CLUSTERS; i++) {
                Cluster c = clusters.get(i);
                distance = Document.distance(c.getCentroid(), document);
                if(distance < min){
                    min = distance;
                    cluster = i;
                }
            }
            document.setCluster(cluster);
            clusters.get(cluster).addDocument(document);
            System.out.println("Dokumen "+document+" masuk ke cluster "+cluster+" dengan jarak "+min+"\n");
        }
    }

    private void calculateCentroids() {
        System.out.println(
                "##############################" + "\n"
                + "Kalkulasi Centroid Baru \n"
                + "##############################");

        for(Cluster cluster : clusters) {
            double max = Double.MAX_VALUE;
            double min = max;
            List<Document> list = cluster.getDocuments();
            Document oldCentroid = cluster.getCentroid();
            list.add(oldCentroid);
            int n_points = list.size();
            int i = 0;

            Document newCentroid = new Document();

            for(Document document : list) {
                double dist = 0;

                for (Document document1 : list) {
                    dist += Document.distance(document, document1);
                }

                if (dist < min) {
                    min = dist;
                    newCentroid = document;
                }

                System.out.println("Dokumen "+document+" memiliki total jarak "+dist+"\n");
                i++;
            }

            cluster.setCentroid(newCentroid);
            documents.add(oldCentroid);
            documents.remove(newCentroid);
            System.out.println(cluster+" => Centroid lama: "+oldCentroid+", centroid baru: "+newCentroid+"\n");
        }
    }
}

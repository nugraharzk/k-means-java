package id.skripsi;

public class Document {

    private String data;
    private String instansi;
    private String status;
    private String alumni;
    private String gaji;
    private String lokasi;
    private String bidang;
    private int cluster;

    public Document() {
    }

    public Document(String data, String instansi, String status, String alumni, String gaji, String lokasi, String bidang) {
        this.data = data;
        this.instansi = instansi;
        this.status = status;
        this.alumni = alumni;
        this.gaji = gaji;
        this.lokasi = lokasi;
        this.bidang = bidang;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getInstansi() {
        return instansi;
    }

    public void setInstansi(String instansi) {
        this.instansi = instansi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAlumni() {
        return alumni;
    }

    public void setAlumni(String alumni) {
        this.alumni = alumni;
    }

    public String getGaji() {
        return gaji;
    }

    public void setGaji(String gaji) {
        this.gaji = gaji;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getBidang() {
        return bidang;
    }

    public void setBidang(String bidang) {
        this.bidang = bidang;
    }

    public int getCluster() {
        return cluster;
    }

    public void setCluster(int cluster) {
        this.cluster = cluster;
    }

    public static double distance(Document centroid, Document document) {
        double dist = 0;
        dist += getDifference(centroid.getInstansi(), document.getInstansi());
        dist += getDifference(centroid.getStatus(), document.getStatus());
        dist += getDifference(centroid.getAlumni(), document.getAlumni());
        dist += getDifference(centroid.getBidang(), document.getBidang());
        dist += getDifference(centroid.getGaji(), document.getGaji());
        dist += getDifference(centroid.getLokasi(), document.getLokasi());
        System.out.println("Dokumen "+document+" dengan centroid "+centroid+" berjarak "+dist);
        return dist;
    }

    private static double getDifference(String instansi, String instansi1) {
        if (instansi.length() != instansi1.length()) {
            return 1;
        } else {
            for (int i = 0; i < instansi.length(); i++) {
                if (instansi.charAt(i) != instansi1.charAt(i)) return 1;
            }
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Data ke-"+data;
    }
}
